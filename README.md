<!--
SPDX-FileCopyrightText: 2021 leirda

SPDX-License-Identifier: WTFPL
-->

Execute `./pingu` in your shell.

*Note that your terminal or terminal emulator should support the `terminfo(5)` database.*
